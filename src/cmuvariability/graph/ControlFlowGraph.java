package cmuvariability.graph;

import nu.xom.Element;

import java.util.*;

/**
 * Created by jeam on 27/10/16.
 */
public class ControlFlowGraph {

    private List<Vertex> vertices;
    private List<Edge> edges;

    public Vertex startVertex;
    public Vertex exitVertex;


    ControlFlowGraph() {
        vertices = new ArrayList<>();
        edges = new LinkedList<>();

        startVertex = new Vertex("START", VertexType.START);
        exitVertex = new Vertex("EXIT", VertexType.EXIT);
    }

    public void addVertex(Vertex vertex) {
        vertices.add(vertex);
    }

    public void addEdge(String type, Vertex s, Vertex t) {
        Edge edge;
        if (type == null)
            edge = new Edge("\"\"", s, t);
        else
            edge = new Edge("\""+type+"\"", s, t);
        edges.add(edge);
    }

    public void addEdge(Vertex s, Vertex t) {
        Edge edge = new Edge(s, t);
        edges.add(edge);
    }

    public Iterator<Edge> getEdgeIterator() {
        return edges.iterator();
    }

    public List<Vertex> getVertices() {
        return vertices;
    }

    public List<Vertex> getVerticesByType(VertexType type) {
        List<Vertex> listOfVertices = new ArrayList<>();

        for (Vertex v: vertices) {
            if(v.getType() == type) {
                listOfVertices.add(v);
            }
        }

        return listOfVertices;
    }

    public List<Edge> getEdges() {
        return edges;
    }

    public List<Edge> getIncomingEdgesByVertex(Vertex v) {
        List<Edge> incomingEdges = new ArrayList<>();

        for (Edge edge: edges) {
            if(edge.getTarget().equals(v)) {
                incomingEdges.add(edge);
            }
        }

        return incomingEdges;
    }

    public List<Edge> getOutgoingEdgesByVertex(Vertex v) {
        List<Edge> outgoingEdges = new ArrayList<>();

        for (Edge edge: edges) {
            if(edge.getSource().equals(v)) {
                outgoingEdges.add(edge);
            }
        }

        return outgoingEdges;
    }

    public List<Edge> getEdgesByVertex(Vertex vertex) {
        List<Edge> edgeList = new ArrayList<>();

        for (Edge edge: edges) {
            if(edge.getSource().equals(vertex) || edge.getTarget().equals(vertex)) {
                edgeList.add(edge);
            }
        }

        return edgeList;
    }

    public boolean removeVertex(Vertex vertex) {
        return vertices.remove(vertex);
    }

    public boolean removeEdge(Edge edge) {
        return edges.remove(edge);
    }

    public void removeEdges(List<Edge> edges) {

        for (Edge edge : edges) {
            this.edges.remove(edge);
        }
    }

    public void clearAll() {
        vertices.clear();
        edges.clear();
    }

    public void addSubCFG(ControlFlowGraph subCFG) {
        vertices.addAll(subCFG.getVertices());
        edges.addAll(subCFG.getEdges());
    }
}
