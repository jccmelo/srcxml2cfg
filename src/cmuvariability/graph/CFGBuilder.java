package cmuvariability.graph;

import cmuvariability.preprocessor.ContextManager;
import nu.xom.Document;
import nu.xom.Element;
import nu.xom.Elements;
import nu.xom.Node;

import java.util.*;
import java.util.concurrent.CopyOnWriteArrayList;

/**
 * Created by jeam on 27/10/16.
 */
public class CFGBuilder {
    private final Document document;
    private ControlFlowGraph cfg;
    private Stack<Vertex> stmtStack;
    private Stack<Vertex> ifdefStack;
    private Map<String, List<Vertex>> functionVerticesMap;
    private StringBuilder dotCFG;
    private ContextManager contextManager;

    public static final String NAMESPACEURI = "http://www.srcML.org/srcML/src";
    public static final String NAMESPACEURI_CPP = "http://www.srcML.org/srcML/cpp";
    public static final String NAMESPACEURI_POS = "http://www.srcML.org/srcML/position";

    public CFGBuilder(Document doc) {
        document = doc;
        cfg = new ControlFlowGraph();
        stmtStack = new Stack<>();
        ifdefStack = new Stack<>();
        functionVerticesMap = new HashMap<>();
        dotCFG = new StringBuilder();
        contextManager = ContextManager.getContext();
    }

    public ControlFlowGraph buildCFG() {
        dotCFG.append("digraph CFG { \n");
        dotCFG.append("compound=true\n");

        ControlFlowGraph cfgFile = new ControlFlowGraph();
        List<String> functionNames = new ArrayList<>();

        Element root = document.getRootElement();
        List<Element> functionNodes = getFunctionNodes(root.getChildElements());
        for (Element function: functionNodes) {

            if(stmtStack.isEmpty()) {
                stmtStack.push(cfg.startVertex);
            }

            List<Element> stmts = getStmtsFromFuncElement(function);
            for (Element currentStmt: stmts) {
                parse(currentStmt);
            }

            // prune graph
            pruneGraph();

            // create .dot subgraph
            Element funcNameNode = function.getFirstChildElement("name", NAMESPACEURI);
            Element parameterListNode = function.getFirstChildElement("parameter_list", NAMESPACEURI);
            String functionName = funcNameNode.getValue();
            String params = parameterListNode.getValue().replaceAll("\\s*", "");

            functionNames.add(functionName+params);
            dotCFG.append(printDotCFG(functionName+parameterListNode.getValue()));

            // store important information
            List<Vertex> vertexList = new ArrayList<>();
            vertexList.addAll(cfg.getVertices());
            functionVerticesMap.put(functionName+params, vertexList);
            cfgFile.addSubCFG(cfg);

            // clear phase
            stmtStack.clear();
            cfg.clearAll();
        }

        dotCFG.append(addEdgeBetweenFunctions(cfgFile, functionNames));
        dotCFG.append("}");

        return cfgFile;
    }


    public void parse(Element stmt) {

        if(stmt.getLocalName().equals("comment"))
            return;

        VertexType parsedStatement = whichControlStmt(stmt.getValue());
        switch (parsedStatement) {
            case IFDEF:
            case ELSE_CPP:
            case ENDIF:
            case IF:
            case ELSE:
            case WHILE:
            case DO_WHILE:
            case FOR:
                process(stmt, parsedStatement);
                break;

            default:
                process(stmt, VertexType.SIMPLE);
        }

    }


    public void process(Element stmt, VertexType vertexType) {

        if(stmtStack.isEmpty())
            return;

        switch (vertexType) {
            case SIMPLE:
                Vertex src = stmtStack.peek();
                Vertex tgt = new Vertex(stmt.getValue(), vertexType);

                if(!src.isCondIf() && !src.isCondIfdef()) { //&& !src.isCondDo()
                    src = stmtStack.pop();
                }
                //else {
                //    edgeType = getEdgeLabel(src);
                //}

                int lineNumber = getStmtLineNumber(stmt);
                Set<String> feat = contextManager.getFeaturesByLine(lineNumber);
                String pc = (feat != null) ? feat.toString().replaceAll("\\[|\\]",""): null;

                setStmtLineNumber(tgt, stmt);
                cfg.addVertex(tgt);
                cfg.addEdge(pc, src, tgt);
                stmtStack.push(tgt);

                break;
            case IFDEF:
                Vertex topp = stmtStack.peek();
                Vertex ifdef = new Vertex(stmt.getValue(), vertexType);

                if(!topp.isCondIf() && !topp.isCondDo() && !topp.isCondIfdef())
                    topp = stmtStack.pop();

                cfg.addVertex(ifdef);
                cfg.addEdge(topp, ifdef);
                stmtStack.push(ifdef);
                ifdefStack.push(ifdef);

                break;
            case ELSE_CPP:

                Vertex ifdefThenTop = stmtStack.pop();
                ifdefStack.push(ifdefThenTop);

                if (!stmtStack.isEmpty() && !stmtStack.peek().isCondIfdef())
                    stmtStack.pop();


                break;
            case ENDIF:
                Vertex t = stmtStack.peek();
                ifdefStack.push(t);

                while (!stmtStack.isEmpty() && t != null && !t.isCondIfdef()) {
                    t = stmtStack.pop();
//                    if(t.isCondIfdef()) {
//                        edgeType = "F";
//                    }
                    //cfg.addEdge(t, endif);
                }

                Vertex endif = new Vertex(stmt.getValue(), vertexType);
                cfg.addVertex(endif);

                int stackSizeUntilFirstIfdef = stackSizeUntilFirstIfdef(ifdefStack);

                if (stackSizeUntilFirstIfdef == 2) { // #ifdef && #else
                    while (!ifdefStack.isEmpty() && !ifdefStack.peek().isCondIfdef()) {
                        cfg.addEdge(ifdefStack.pop(), endif);
                    }
                } else { // only #ifdef
                    int loop = stackSizeUntilFirstIfdef+1;
                    Vertex head = null;
                    while (!ifdefStack.isEmpty() && loop != 0) {
                        head = ifdefStack.pop();
                        cfg.addEdge(head, endif);
                        loop--;
                    }

                    if (!ifdefStack.isEmpty())
                        ifdefStack.push(head);
                }

                clearStackUntilFirstIfdef(ifdefStack);
                stmtStack.push(endif);

                break;
            case IF:
                Element conditionNode = stmt.getFirstChildElement("condition", NAMESPACEURI);

                Vertex top = stmtStack.peek();
                Vertex condIf = new Vertex(stmt.getLocalName()+" "+conditionNode.getValue(), vertexType);

                if(!top.isCondIf() && !top.isCondDo())
                    top = stmtStack.pop();

                setStmtLineNumber(condIf, stmt);
                cfg.addVertex(condIf);
                cfg.addEdge(top, condIf);
                stmtStack.push(condIf);

                // handle then branch, <then> [<block>]
                Element thenElement = stmt.getFirstChildElement("then", NAMESPACEURI);
                if (thenElement != null) {

                    processStmtsUnderBranch(thenElement);


                    Vertex s1 = stmtStack.peek();
                    while (!stmtStack.isEmpty() && !stmtStack.peek().isCondIf()) {
                        stmtStack.pop();
                    }

                    // handle else branch, [<else>] [<block>]
                    Element elseElement = stmt.getFirstChildElement("else", NAMESPACEURI);
                    if (elseElement != null) {
                        processStmtsUnderBranch(elseElement);
                    }


                    createVertexAndEdge(s1, vertexType);
                }

                break;

            case FOR:
                /* for (s1; s2; s3) { S }
                 * CFG(for) = s1 ; if(s2) then CFG(S); s3; else JOIN;
                 * s1 = forInit ; s2 = forCondition ; S = forBlock ; s3 = forIncr
                 */
                Element forControl = stmt.getFirstChildElement("control", NAMESPACEURI);
                Element forInit = forControl.getFirstChildElement("init", NAMESPACEURI);
                Element forCondition = forControl.getFirstChildElement("condition", NAMESPACEURI);
                Element forIncr = forControl.getFirstChildElement("incr", NAMESPACEURI);


                if (!(forInit.getValue().equals("") || forInit.getValue().equals(";")))
                    parse(forInit);

                // create if vertex and its edge
                Vertex topV = stmtStack.peek();
                Vertex condV = new Vertex("if " + forCondition.getValue(), VertexType.IF);
                setStmtLineNumber(condV, forCondition);

                if(!topV.isCondIf())
                    topV = stmtStack.pop();

                cfg.addVertex(condV);
                cfg.addEdge(topV, condV);
                stmtStack.push(condV);

                processStmtsUnderBranch(stmt);

                if (!(forIncr.getValue().equals("") || forIncr.getValue().equals(";")))
                    parse(forIncr);

                connectLastVertexOnStackWithConditionVertex(condV);

                while (!stmtStack.isEmpty() && stmtStack.peek().hashCode() != condV.hashCode()) {
                    stmtStack.pop();
                }

                break;

            case WHILE:

                processWhile(stmt, vertexType);
                break;

            case DO_WHILE:

                // do
                Vertex vTop = stmtStack.peek();
                Vertex vDo = new Vertex("DO", VertexType.DO_WHILE);

                if(!vTop.isCondIf() && !vTop.isCondDo())
                    vTop = stmtStack.pop();

                cfg.addVertex(vDo);
                cfg.addEdge(vTop, vDo);
                stmtStack.push(vDo);

                processStmtsUnderBranch(stmt);

                // while
                Element whileCond = stmt.getFirstChildElement("condition", NAMESPACEURI);
                Vertex vIf = new Vertex("if " + whileCond.getValue(), VertexType.IF);
                setStmtLineNumber(vIf, whileCond);

                connectLastVertexOnStackWithConditionVertex(vIf);

                cfg.addEdge(vIf, vDo);

                /*if(!stmtStack.isEmpty()) {
                    vTop = stmtStack.peek();
                    while (!vTop.isCondDo()) {
                        vTop = stmtStack.pop();
                    }
                }
*/
//                vDo = stmtStack.pop();


                break;
        }
    }

    private void processWhile(Element stmt, VertexType vertexType) {
        Element cond = stmt.getFirstChildElement("condition", NAMESPACEURI);
        Vertex vertexIf = new Vertex("if " + cond.getValue(), VertexType.IF);
        setStmtLineNumber(vertexIf, cond);

        connectLastVertexOnStackWithConditionVertex(vertexIf);

        processStmtsUnderBranch(stmt);

        Vertex s1 = stmtStack.peek();
        while (!stmtStack.isEmpty() && !stmtStack.peek().isCondIf()) {
            stmtStack.pop();
        }

        createVertexAndEdge(s1, vertexType);
    }

    private void processStmtsUnderBranch(Element element) {
        Element blockThen = element.getFirstChildElement("block", NAMESPACEURI);
        if(blockThen != null) {
            List<Element> thenStmts = getStmtsFromBlockElement(blockThen);
            for (Element currstmt: thenStmts) {
                parse(currstmt);
            }
        } else {
            for (int i = 0; i < element.getChildElements().size(); i++) {
                parse(element.getChildElements().get(i));
            }
        }
    }

    private List<Element> getStmtsFromBlockElement(Element blockElement) {
        List<Element> stmts = new ArrayList<>();

        Elements allStmts = blockElement.getChildElements();

        for (int i = 0; i < allStmts.size(); i++) {
            Element stmt = allStmts.get(i);

            if(!stmt.getLocalName().equals("position"))
                stmts.add(stmt);
        }

        return stmts;

    }

    private void createVertexAndEdge(Vertex vertex, VertexType type) {
        if(!stmtStack.isEmpty()) {
            Vertex s2 = stmtStack.peek();
            while (!stmtStack.isEmpty() && !stmtStack.peek().isCondIf()) {
                stmtStack.pop();
            }

            stmtStack.pop(); // delete condition
            Vertex join = new Vertex("JOIN (" + vertex.getData()+","+ s2.getData()+ ")", VertexType.JOIN);
            cfg.addVertex(join);
            cfg.addEdge(s2, join);
            stmtStack.push(join);

            if(type == VertexType.IF)
                cfg.addEdge(vertex, join);
            else
                cfg.addEdge(vertex, s2);
        }
    }

    private void connectLastVertexOnStackWithConditionVertex(Vertex condVertex) {
        Vertex vTop = stmtStack.peek();
        if(!vTop.isCondIf() && !vTop.isCondDo())
            vTop = stmtStack.pop();

        cfg.addVertex(condVertex);
        cfg.addEdge(vTop, condVertex);
        stmtStack.push(condVertex);
    }

    private String getEdgeLabel(Vertex src) {
        String edgeType;
        if(cfg.getOutgoingEdgesByVertex(src).size() < 1) {
            edgeType = "T";
        } else {
            edgeType = "F";
        }

        return edgeType;
    }

    private VertexType whichControlStmt(String line) {
        VertexType returnVal = VertexType.SIMPLE;
        if (isIf(line)) returnVal = VertexType.IF;
        else if (isDoWhile(line)) returnVal = VertexType.DO_WHILE;
        else if (isFor(line)) returnVal = VertexType.FOR;
        else if (line.contains("#ifdef")) returnVal = VertexType.IFDEF;
        else if (line.contains("#else")) returnVal = VertexType.ELSE_CPP;
        else if (line.contains("#endif")) returnVal = VertexType.ENDIF;
        else if (line.contains("while")) returnVal = VertexType.WHILE;
        else if (line.contains("else")) returnVal = VertexType.ELSE;
        else if(line.isEmpty()) returnVal = VertexType.EMPTY;
        return returnVal;
    }

    private boolean isIf(String line) {

        if(line.contains("if")) {
            String ifStmt = line.substring(0, line.indexOf(")")+1);
            if (ifStmt.matches("^\\s*if\\s*(.*)"))
                return true;
        }

        return false;
    }

    private boolean isDoWhile(String line) {

        if (line.contains("{")) {
            String doStmt = line.substring(0, line.indexOf("{")+1);

            if (doStmt.matches("^\\s*do\\s*\\{"))
                return true;
        }

        return false;

    }

    private boolean isFor(String line) {

        if (line.contains("{")) {
            String forStmt = line.substring(0, line.indexOf("{")+1);

            if (forStmt.matches("^\\s*for\\s*(.*)\\s*\\{"))
                return true;
        }
        //else if(line.contains("for")) {
        //    return true;
        //}

        return false;

    }


    public List<Element> getFunctionNodes(Elements children) {
        List<Element> funcNodes = new ArrayList<>();

        for (int i = 0; i < children.size(); i++) {
            Element currentChild = children.get(i);

            if(currentChild.getLocalName().equalsIgnoreCase("function")) {
                funcNodes.add(currentChild);
            }
        }

        return funcNodes;
    }

    public List<Element> getStmtsFromFuncElement(Element functionElement) {
        List<Element> stmts = new ArrayList<>();

        for (int j = 0; j < functionElement.getChildCount(); j++) {
            Node node = functionElement.getChild(j);

            if (node instanceof Element) {
                Element element = (Element) node;
                if (element.getLocalName().equals("block")) { // function body

                    Elements allStmts = element.getChildElements();

                    for (int i = 0; i < allStmts.size(); i++) {
                        Element stmt = allStmts.get(i);
                        if(!stmt.getLocalName().equals("position"))
                            stmts.add(stmt);
                    }
                }
            }
        }

        return stmts;
    }

    private void pruneGraph() {
        pruneGraphByVertexType(VertexType.JOIN);
        pruneGraphByVertexType(VertexType.DO_WHILE);
        pruneGraphByVertexType(VertexType.ENDIF);
        pruneGraphByVertexType(VertexType.IFDEF);
    }

    private void pruneGraphByVertexType(VertexType type) {
        List<Vertex> vertices = cfg.getVerticesByType(type);

        for (Vertex v : vertices) {
            List<Edge> incomingEdges = cfg.getIncomingEdgesByVertex(v);
            List<Edge> outgoingEdges = cfg.getOutgoingEdgesByVertex(v);

            for (Edge incomingEdge : incomingEdges) {
                for (Edge outgoingEdge: outgoingEdges) {
                    String pc = outgoingEdge.getType().replaceAll("\"","");
                    cfg.addEdge(pc, incomingEdge.getSource(), outgoingEdge.getTarget());
                }
            }
            cfg.removeEdges(incomingEdges);
            cfg.removeEdges(outgoingEdges);
            cfg.removeVertex(v);
        }
    }

    private void collapseSingleStmtsIntoBasicBlocks() {
        List<Vertex> tmpVertices = new ArrayList<>();
        List<Vertex> verticesToRemove = new ArrayList<>();

        List<Vertex> vertices = new CopyOnWriteArrayList<>(cfg.getVertices()); // cfg.getVertices();
        for (Vertex vertex : vertices) {
            tmpVertices.add(vertex);

            if (cfg.getOutgoingEdgesByVertex(vertex).size() > 1) {
                createBasicBlock(tmpVertices);
                tmpVertices.clear();
            }
        }
    }

    private void createBasicBlock(List<Vertex> vertices) {
        List<Edge> condEdges = new ArrayList<>();
        Vertex condVertex = null;

        StringBuilder data = new StringBuilder();
        for (Vertex v : vertices) {
            condEdges = cfg.getOutgoingEdgesByVertex(v);
            condVertex = v;

            data.append(v.getData()+"\n");
            cfg.removeVertex(v);
            cfg.removeEdges(cfg.getEdgesByVertex(v));
        }

        Vertex basicBlock = new Vertex(data.toString());
        cfg.addVertex(basicBlock);
        for (Edge e : condEdges) {
            if (e.getTarget().hashCode() != condVertex.hashCode()) {
                cfg.addEdge(basicBlock, e.getTarget());
            }
        }
    }

    public void setStmtLineNumber(Vertex v, Element stmt) {
        v.setLineNumber(getStmtLineNumber(stmt));
    }

    public int getStmtLineNumber(Element stmt) {
        int lineNumber;

        if (stmt.getAttributeCount() == 0) {
            Element pos = stmt.getFirstChildElement("position", NAMESPACEURI_POS);
            if(pos != null) {
                lineNumber = Integer.parseInt(pos.getAttribute(0).getValue());
            } else {

                if(stmt.getLocalName().equals("macro")) {
                    Element macro = stmt.getFirstChildElement("name", NAMESPACEURI);
                    Element position = macro.getFirstChildElement("position", NAMESPACEURI_POS);
                    lineNumber = Integer.parseInt(position.getAttribute(0).getValue());
                } else {
                    // exceptional case for increment in a for loop
                    Element incr = (Element) stmt.getChild(0).getChild(0);
                    Element position = incr.getFirstChildElement("position", NAMESPACEURI_POS);
                    lineNumber = Integer.parseInt(position.getAttribute(0).getValue());
                }
            }
        } else {
            lineNumber = Integer.parseInt(stmt.getAttribute(0).getValue());
        }

        return lineNumber;
    }

    //931919113 -> 999966131[lhead=cluster_trace_dump_stack]
    //func call hashcode -> first node[lhead=cluster_function_name]
    private String addEdgeBetweenFunctions(ControlFlowGraph graph, List<String> functionNames) {
        StringBuilder outputDot = new StringBuilder();

        List<Vertex> vertexList = graph.getVertices();
        // TODO: get only vertices that are function calls, not all vertices

        for (Vertex v : vertexList) {

            String funcCall = getFunctionCallFromStmt(v.getData());
            if(funcCall.isEmpty()) {
                continue;
            }

            for (String funcName : functionNames) {

                if(funcCall.contains(funcName.substring(0, funcName.indexOf("(")+1))
                        && !functionVerticesMap.get(funcName).isEmpty()) {

                    Vertex firstVertex = functionVerticesMap.get(funcName).get(0);

                    int lineNumber = v.getLineNumber();
                    Set<String> feat = contextManager.getFeaturesByLine(lineNumber);
                    String pc = (feat != null) ? feat.toString().replaceAll("\\[|\\]",""): null;

                    int line2 = firstVertex.getLineNumber();
                    Set<String> feat2 = contextManager.getFeaturesByLine(line2);
                    String pc2 = (feat2 != null) ? feat2.toString().replaceAll("\\[|\\]",""): null;

                    if(pc == null && pc2 == null) {
                        outputDot.append(v.hashCode() + " -> " + firstVertex.hashCode() + "[lhead=\"cluster_" + funcName + "\", label=\"" + "\"]\n");
                    } else if(pc != null && !pc.equalsIgnoreCase(pc2)) {
                        outputDot.append(v.hashCode() + " -> " + firstVertex.hashCode() + "[lhead=\"cluster_" + funcName + "\", label=\"" + pc + ", " + pc2 + "\"]\n");
                    } else {
                        if(pc == null) {
                            pc = pc2;
                        }
                        outputDot.append(v.hashCode() + " -> " + firstVertex.hashCode() + "[lhead=\"cluster_" + funcName + "\", label=\"" + pc + "\"]\n");
                    }
                }
            }
        }

        return outputDot.toString();
    }

    private String getFunctionCallFromStmt(String stmt) {
        String callStmt;

        if (stmt.matches("^(\\s*\\w+)?\\s*.+\\s*=\\s*\\w+\\s*\\(.*\\)\\s*;")) {
            callStmt = stmt.substring(stmt.indexOf("=") + 1, stmt.indexOf("(") + 1);
        } else if (stmt.matches("^\\s*.+\\s*\\(.*\\)\\s*;")) {
            callStmt = stmt.substring(0, stmt.indexOf("(")+1);
        } else if (stmt.matches("^\\s*if\\s*\\(\\s*(!)?\\w+\\s*\\(.*\\)\\s*\\)")) {
            callStmt = stmt.substring(stmt.indexOf("(") + 1, stmt.indexOf(")") + 1);
            callStmt = callStmt.substring(0, callStmt.indexOf("(")+1);
        } else {
            callStmt = "";
        }

        return callStmt.trim();
    }

    public StringBuilder printDotCFG(String functionName) {
        List<Edge> edges = cfg.getEdges();
        StringBuilder outputDot = new StringBuilder();


        if(functionName.isEmpty()) {
            outputDot.append("digraph CFG { \n");
        } else {
            outputDot.append("subgraph \"cluster_" + functionName.replaceAll("\\s*", "") + "\"{ \n");
            outputDot.append("label = \""+ functionName +"\"\n");
        }

        // declare ID labels
        List<Vertex> vertices = cfg.getVertices();
        for (Vertex v : vertices) {
            String vID = String.valueOf(v.hashCode());
            if(v.isCondIf() || v.isCondIfdef()) {
                //outputDot.append(vID + "[shape=diamond,label=\""+ v.getLineNumber() + "\"]"+ "\n");
                outputDot.append(vID + "[shape=diamond,label=\""+ v.getData().trim().replaceAll("\"", "\'") + "\"]"+ "\n");
            } else {
                //outputDot.append(vID + "[label=\""+ v.getLineNumber() + "\"]"+ "\n");
                outputDot.append(vID + "[label=\""+ v.getData().trim().replaceAll("\"", "\'") + "\"]"+ "\n");
            }
        }

        // add arrows
        for (Edge edge: edges) {
            Vertex source = edge.getSource();
            Vertex target = edge.getTarget();

            if(source.getType() == VertexType.START)
                continue;

            outputDot.append(source.hashCode() + "  ->  " + target.hashCode()+ "[label=" + edge.getType() + "]\n");
        }

        outputDot.append("}\n");

        return outputDot;
    }

    public Map<String, List<Vertex>> getFunctionVerticesMap() {
        return functionVerticesMap;
    }

    public StringBuilder getDotCFG() {
        return dotCFG;
    }

    private int stackSizeUntilFirstIfdef(Stack<Vertex> stack) {
        Stack<Vertex> auxStack = new Stack<>();
        auxStack.addAll(stack);

        int size = 0;

        while (!auxStack.isEmpty()) {
            Vertex top = auxStack.pop();
            if(top.getType() == VertexType.IFDEF)
                return size;

            size++;
        }

        return size;
    }

    private void clearStackUntilFirstIfdef(Stack<Vertex> stack) {
        while (!stack.isEmpty()) {
            Vertex top = stack.pop();
            if(top.getType() == VertexType.IFDEF) {
                return;
            }
        }
    }
}
