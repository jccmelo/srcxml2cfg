package cmuvariability.graph;

/**
 * Created by jeam on 27/10/16.
 */
public enum VertexType {
    START(-1), EXIT(-1), EMPTY(-1), JOIN(-1), SIMPLE(0), IF(1), ELSE(2), IFDEF(3), ELSE_CPP(3), ENDIF(3), FOR(4), WHILE(5), DO_WHILE(6);

    private int value;

    VertexType(int value) {
        this.value = value;
    }

    public int getValue() {
        return value;
    }
};
