package cmuvariability.graph;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

/**
 * Created by jeam on 27/10/16.
 */
public class Vertex {
    private String condition;
    private VertexType type;
    private String data;
    private int lineNumber;
    private int column;

    private List<Vertex> parents;
    private List<Edge> edges;
    private boolean visited = false;

    public Vertex(String data) {
        this.data = data;
        edges = new LinkedList<>();
        parents = new ArrayList<>();
    }

    public Vertex(String data, VertexType type) {
        this.data = data;
        this.type = type;
        edges = new LinkedList<>();
        parents = new ArrayList<>();
    }

    /*public Vertex(String data, VertexType type, int lineNumber) {
        this.data = data;
        this.type = type;
        this.lineNumber = lineNumber;
        edges = new LinkedList<>();
        parents = new ArrayList<>();
    }*/

    public String getCondition() {
        return condition;
    }

    public void setCondition(String condition) {
        this.condition = condition;
    }

    public VertexType getType() {
        return type;
    }

    public void setType(VertexType type) {
        this.type = type;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }

    public int getLineNumber() {
        return lineNumber;
    }

    public void setLineNumber(int lineNumber) {
        this.lineNumber = lineNumber;
    }

    public int getColumn() {
        return column;
    }

    public void setColumn(int column) {
        this.column = column;
    }

    public List<Edge> getEdges() {
        return edges;
    }

    public void setEdges(List<Edge> edges) {
        this.edges = edges;
    }

    public void setEdge(Edge edge) {
        this.edges.add(edge);
    }

    public boolean isVisited() {
        return visited;
    }

    public void setVisited(boolean visited) {
        this.visited = visited;
    }

    public List<Vertex> getParents() {
        return parents;
    }

    public void setParents(List<Vertex> parents) {
        this.parents = parents;
    }

    public void setParent(Vertex parent) {
        this.parents.add(parent);
    }

    public Boolean isCondIf() {
        return this.type==VertexType.IF;
    }

    public Boolean isCondIfdef() {
        return this.type==VertexType.IFDEF;
    }

    public Boolean isCondWhile() {
        return this.type==VertexType.WHILE;
    }

    public Boolean isCondDo() {
        return this.type==VertexType.DO_WHILE;
    }
}
