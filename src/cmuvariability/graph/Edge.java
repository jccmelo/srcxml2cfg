package cmuvariability.graph;

/**
 * Created by jeam on 27/10/16.
 */
public class Edge {
    private String type;
    private Vertex source;
    private Vertex target;

    private String condition;

    public Edge(String type, Vertex source, Vertex target) {
        this.type = type;
        this.source = source;
        this.target = target;
    }

    public Edge(Vertex source, Vertex target) {
        this.type = "\"\"";
        this.source = source;
        this.target = target;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Vertex getSource() {
        return source;
    }

    public void setSource(Vertex source) {
        this.source = source;
    }

    public Vertex getTarget() {
        return target;
    }

    public void setTarget(Vertex target) {
        this.target = target;
    }

    public String getCondition() {
        return condition;
    }

    public void setCondition(String condition) {
        this.condition = condition;
    }
}
