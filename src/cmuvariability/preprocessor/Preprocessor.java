package cmuvariability.preprocessor;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.*;
import java.util.regex.MatchResult;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


public class Preprocessor {

	private String defs = ""; // defs are the features (e.g., DEBUG, LOGGING)
	private ContextManager context = ContextManager.getContext();

	public Preprocessor() {
	}

	public StringBuilder execute() throws Exception {
		if (context.getSrcfile() == null) {
			throw new Exception("Input source file is not set. Set it via ContextManager.");
		}

		try {
			return preprocess();
		} catch (IOException e) {
			throw new Exception("IO error while preprocessing", e);
		}
	}

	private StringBuilder preprocess() throws IOException {
		StringBuilder out = new StringBuilder();
		BufferedReader br = null; // for reading from file
		
		try {
			br = new BufferedReader(new FileReader(context.getSrcfile()));

			/**
			 * Gets the defined features, split them by "," and, finally, remove
			 * all duplicate white spaces
			 */		
			HashSet<String> set = new HashSet<>(Arrays.asList(defs.replaceAll("\\s+", "").split(",")));

			// Compiles the regex then sets the pattern
			Pattern pattern = Pattern.compile(Tag.regex, Pattern.CASE_INSENSITIVE);

			String line;
            int lineNumber = 0; // for counting the line number
			int currentLevel = 0; // for controling the tags (e.g., ifdefs and
									// endifs)
			int removeLevel = -1; // if -1 can write, otherwise cannot
			boolean skip = false; // this flag serves to control code within a
									// certain feature

			// reading line-by-line from input file
			while ((line = br.readLine()) != null) {
                lineNumber++;
				/**
				 * Creates a matcher that will match 
				 * the given input against this pattern.
				 */
				Matcher matcher = pattern.matcher(line);

				/**
				 * Matches the defined pattern with the current line
				 */
				if (matcher.matches()) {					
					/**
					 * MatchResult is unaffected by subsequent operations
					 */
					MatchResult result = matcher.toMatchResult();
					String dir = result.group(1).toLowerCase().replace(" ", ""); // cpp directive
					String param = result.group(2); // feature expression
					
					out.append(line + "\n");
					
					if (Tag.IFDEF.equals(dir) || Tag.IF.equals(dir) || Tag.ELIF.equals(dir)) {
						//adds ifdef X on the stack
						context.addDirective(dir + " " + removeWhitespace(param));

						// verifies if the feature was defined
						if (defs.replaceAll("\\s+", "").length() > 0) {
							skip = !set.contains(param);
						} else {
							skip = false;
						}

						if (removeLevel == -1 && skip) {
							removeLevel = currentLevel;
						}
						currentLevel++;
						continue;
					} else if (Tag.IFNDEF.equals(dir)) {
						context.addDirective(dir + " " + param);
						
						if (defs.replaceAll("\\s+", "").length() > 0) {
							skip = set.contains(param);
						} else {
							skip = false;
						}
						
						if (removeLevel == -1 && skip) {
							removeLevel = currentLevel;
						}
						currentLevel++;
						continue;
					} else if (Tag.ELSE.equals(dir)) {
                        String top = context.getTopDirective();
                        context.removeTopDirective();

                        if(top.contains(Tag.IFDEF))
                            top = top.replace(Tag.IFDEF, Tag.IFNDEF);
                        else if(top.contains(Tag.IFNDEF))
                            top = top.replace(Tag.IFNDEF, Tag.IFDEF);

                        context.addDirective(top);

                        currentLevel--;
						if (currentLevel == removeLevel) {
							removeLevel = -1;
						}
						if (removeLevel == -1 && !skip) {
							removeLevel = currentLevel;
						}
						currentLevel++;
						continue;
					} else if (Tag.ENDIF.equals(dir)) {

						if (context.stackIsEmpty()) {
							System.out.println("#endif encountered without "
									+ "corresponding #if or #ifdef or #ifndef");
							return null;
						}

						context.removeTopDirective();

						currentLevel--;
						if (currentLevel == removeLevel) {
							removeLevel = -1;
						}
						continue;
					}
				} else {
					//out.append(line + "\n");
                    // verifies if the current line does not have text (code)
                    if(!line.trim().isEmpty()){
                        // Add information to the map: feature expression -> line number.
                        addInfoOnMapping(context, lineNumber);
                    }
				}
//				if (removeLevel == -1 || currentLevel < removeLevel) {
//					bw.write(line);
//					bw.newLine();
//				}
			}
		} finally {
			if (br != null) {
				br.close();
			}
		}
		
		return out;
	}

    private void addInfoOnMapping(ContextManager context, Integer infoLine){
        // verifica pelo contexto se o a linha atual pertence alguma
        // feature se sim, add no map. Caso contrario, faz nada.
        Stack<String> auxStack = new Stack<>();

        // copy stack
        for (int i = 0; i < context.stackSize(); i++) {
            auxStack.add(context.stackDirectives.get(i));
        }

        while (!auxStack.isEmpty()) {
            // gets feature's name
            String feature = auxStack.peek().split(" ")[1];

            if (auxStack.peek().contains(Tag.IFNDEF)) {
                feature = "¬" + feature;
            }

            // add info about line number of a certain feature
            context.addFeatureInfo(feature, infoLine);
            context.addInfo(infoLine, feature);

            auxStack.pop();
        }

        auxStack.clear();
    }
	
	public List<String> getFeatureNames(String featureExpression) {
		List<String> featureNames = new ArrayList<>();
		
		featureExpression = removeComments(featureExpression);
		featureExpression = removeWhitespace(featureExpression);
		featureExpression = removeKeywords(featureExpression);
		featureExpression = removeParentheses(featureExpression);
		featureExpression = removeRelationalOperators(featureExpression);
		featureExpression = removeLogicalOperators(featureExpression);
		
		String[] features = featureExpression.split(",");
		for (String feature : features) {
			featureNames.add(feature);	
		}
		
		return featureNames;
	}
	
	private String removeComments(String featureExpression) {
		int idx = featureExpression.indexOf("//");
		if (idx != -1)
			return featureExpression.substring(0, idx);
		
		return featureExpression;
	}
	
	private String removeRelationalOperators(String featureExpression) {
		String relationalOpsRegex = "(==|!=|>|>=|<|<=)";
		featureExpression = featureExpression.replaceAll(relationalOpsRegex+"\\d+", "");
		featureExpression = featureExpression.replaceAll("\\d+"+relationalOpsRegex, "");
		
		return featureExpression;
	}

	private String removeLogicalOperators(String featureExpression) {
		return featureExpression.replaceAll("&&|\\|\\|", ",");
	}

	private String removeParentheses(String featureExpression) {
		return featureExpression.replaceAll("\\(|\\)|!", "");
	}

	private String removeKeywords(String featureExpression) {
		return featureExpression.replaceAll("defined|def|ndef", "");
	}

	private String removeWhitespace(String featureExpression) {
		return featureExpression.replaceAll("\\s", "");
	}
	
	public String getDefs() {
		return defs;
	}

	public void setDefs(String defs) {
		this.defs = defs;
	}
	
	public void run(String dirpath) {
		ContextManager manager = ContextManager.getContext();
		
		try {
			File dir = new File(dirpath);
			
			if (dir.isDirectory()) {
				manager.setDestfile(dirpath+"REconfig.c");
				File[] files = dir.listFiles();

				for (File file : files) {
					try {
						String filepath = file.getCanonicalPath();
						if (filepath.endsWith(".c") || filepath.endsWith(".h")) {
							manager.setSrcfile(filepath);
						}
						execute();
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
			} else {
				manager.setDestfile(dir.getParent() + File.separator + "REconfig.c");
				manager.setSrcfile(dirpath);
			}

			execute();
			//writeTransformedFeatureNamesToFile(); //output: REconfig.c with all features listed
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	
	public StringBuilder runFile(String filepath) {
		ContextManager manager = ContextManager.getContext();
		
		manager.setSrcfile(filepath);
		try {
			return execute();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
	public static void main(String[] args) {

		String dirpath = "/Users/jeam/IdeaProjects/srcxml2cfg/example/main.c"; //input

		Preprocessor pp = new Preprocessor();
		pp.runFile(dirpath);

        pp.context.printFeatureLineMap();

	}
}