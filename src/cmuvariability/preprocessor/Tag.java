package cmuvariability.preprocessor;

public final class Tag {

	public static final String comment = "//";
	public static final String IF = "#if";
	public static final String IFDEF = "#ifdef";
	public static final String IFNDEF = "#ifndef";
	public static final String ELSE = "#else";
	public static final String ELIF = "#elif";
	public static final String ENDIF = "#endif";
	public static final String INCLUDE = "#include";

	public static final String regex = "^\\s*"+"(" + IFDEF + "|"
			+ IFNDEF + "|" + ELIF + "|" + ELSE + "|"
			+ IF + "|"+ ENDIF + "|" + INCLUDE
			+ ")\\s*(.*)\\s*$";
	
}
