package cmuvariability.util;

import nu.xom.Builder;
import nu.xom.Document;
import nu.xom.ParsingException;

import java.io.*;

/**
 * Created by jeam on 25/10/16.
 */
public class XMLUtils {


    public String dotToPdf(String inputFile) {
        // create dir for store xml files
        String outputPdfFile = inputFile + ".pdf";

        //run srcML
        if (new File(inputFile).isFile()) {
            try {
                ProcessBuilder processBuilder = new ProcessBuilder("/usr/local/bin/dot", "-Tpdf", inputFile, "-o", outputPdfFile);
                Process process = processBuilder.start();
                process.waitFor();

            } catch (IOException e) {
                e.printStackTrace();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        } else {
            System.out.println("File does not exist: " + inputFile);
        }
        return outputPdfFile;
    }


    /**
     * @param inputFile file that need to be parsed by srcML
     * @return path of XML file
     * @throws IOException e
     */
    public String transformSrcCodeToXML(String inputFile) {
        // create dir for store xml files
        String outXmlFile = inputFile + ".xml";

        //run srcML
        if (new File(inputFile).isFile()) {
            try {
                ProcessBuilder processBuilder = new ProcessBuilder("/Users/jeam/srcML/bin/srcml", "--position", inputFile, "-o", outXmlFile);
                //ProcessBuilder processBuilder = new ProcessBuilder("/Users/jeam/srcML/bin/srcml", inputFile, "-o", outXmlFile);
                Process process = processBuilder.start();
                process.waitFor();

            } catch (IOException e) {
                e.printStackTrace();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        } else {
            System.out.println("File does not exist: " + inputFile);
        }
        return outXmlFile;
    }

    /**
     * parse xml file to DOM.
     *
     * @param xmlFilePath path of xml file
     */
    public static Document getXmlDom(String xmlFilePath) {
        XMLUtils io = new XMLUtils();
        Document doc = null;
        try {
            Builder builder = new Builder();
            File file = new File(xmlFilePath);

            String xml = io.readResult(xmlFilePath);
            if (xml.length() > 0) {
                doc = builder.build(file);

            }
        } catch (ParsingException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return doc;
    }

    /**
     * read the content of the file from filePath, and ready for comparing
     *
     * @param filePath file path
     * @return content of the file
     * @throws IOException e
     */
    public String readResult(String filePath) throws IOException {
        BufferedReader result_br = new BufferedReader(new FileReader(filePath));
        String result = "";
        try {
            StringBuilder sb = new StringBuilder();
            String line = result_br.readLine();

            while (line != null) {

                sb.append(line);
                sb.append(System.lineSeparator());

                line = result_br.readLine();
            }
            result = sb.toString();
        } finally {
            result_br.close();
        }
        return result;
    }

    public void writeXMLStringToFile(String content, String filepath) {

        try {
            File file = new File(filepath);

            if (!file.exists()) {
                file.getParentFile().mkdir();
                file.createNewFile();
            }

            FileWriter fw = new FileWriter(file.getAbsoluteFile(), true);
            BufferedWriter bw = new BufferedWriter(fw);
            bw.write(content);
            bw.close();

        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    public static void main(String[] args) {
        String inputCodeFile = "/Users/jeam/IdeaProjects/srcxml2cfg/p1.c";

        XMLUtils xmlutils = new XMLUtils();
        String xmlFilePath = xmlutils.transformSrcCodeToXML(inputCodeFile);

        System.out.println(xmlFilePath);

        //xmlutils.writeXMLStringToFile(xml);
    }
}
