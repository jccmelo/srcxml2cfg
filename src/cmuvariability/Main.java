package cmuvariability;

import cmuvariability.graph.CFGBuilder;
import cmuvariability.preprocessor.Preprocessor;
import cmuvariability.util.XMLUtils;
import nu.xom.*;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

public class Main {

    public static void main(String[] args) {

        Builder builder = new Builder();
        try {
            String inputPath = "/Users/jeam/Documents/PhD repo/srcxml2cfg/";
            String inputCode = "example/tcrypt.c";
            String inputCodeFile = inputPath+inputCode;

            Preprocessor pp = new Preprocessor();
            pp.runFile(inputCodeFile);

            // get XML file from source code via SrcML
            XMLUtils xmlutils = new XMLUtils();
            String xmlFilePath = xmlutils.transformSrcCodeToXML(inputCodeFile);

            // read a xml file
            File xmlfile = new File(xmlFilePath);
            Document doc = builder.build(xmlfile);

            // build the CFG
            CFGBuilder cfgBuilder = new CFGBuilder(doc);
            cfgBuilder.buildCFG();
            String dotCFG = cfgBuilder.getDotCFG().toString();
            //System.out.println(dotCFG);


            // save it into .dot and then .pdf
            File dotFile = new File(inputCode+"_CFG.dot");
            FileOutputStream out = new FileOutputStream(dotFile);
            out.write(dotCFG.getBytes());
            out.close();

            //dot -Tpdf cfg.dot -o outfile.pdf
            xmlutils.dotToPdf(dotFile.getAbsolutePath());


        } catch (ParsingException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }
}
